void main(List<String> args) {
  var appNames = [
    "FNB",
    "Bookly",
    "Super Sport",
    "EskomSePush",
    "Ikhokha",
    "Zulzi",
    "Ctrl",
    "Vula",
    "Aa",
    "Shyft"
  ];

  //a)
  appNames.sort();
  for (int i = 0; i < appNames.length; i++) {
    print(appNames.elementAt(i));
  }

  //b)
  print(
      "Winning App of 2017 is: ${appNames.elementAt(9)}\nWinning App of 2018 is: ${appNames.elementAt(2)}");

  //c)
  print("Total of Apps is: ${appNames.length}");
}
