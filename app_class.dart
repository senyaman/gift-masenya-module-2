class App {
  String appName;
  String sector;
  String developer;
  int yearWon;

  App(this.appName, this.sector, this.developer, this.yearWon);

  //b)
  String toUpper() {
    return appName.toUpperCase();
  }
}

void main(List<String> args) {
  var appDetails =
      App("Ambani Africa", "Gaming Solution", "Mukundi Lambani", 2021);
  //a)
  print("App name: " +
      appDetails.appName +
      "\n"
          "Category: " +
      appDetails.sector +
      "\n"
          "Developer: " +
      appDetails.developer +
      "\n"
          "Year Won: " +
      "${appDetails.yearWon}");

  print(appDetails.toUpper());
}
